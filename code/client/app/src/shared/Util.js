const Util = {
    /**
     * Utility function to convert the JSON structure to a more generic presentation format
     * @param orgRoot
     * @returns {Array|Object|*}
     */
    flattenStructure (orgRoot){
        return orgRoot.map((item) => {
            // Get object name
            const keys = Object.keys(item);
            // construct presentation format recursively
            return {
                name: keys[0],
                position: item[keys[0]].position,
                employees: this.flattenStructure(item[keys[0]].employees) // recursive call
            };
        });
    }
};

export default Util