import React from 'react';
import { Util }  from './shared';

it('Resolves a structure', () => {
    let structure = {'person': {position: 'position',employees:[]}};
    let result = Util.flattenStructure([structure]);
    expect(result.toString()).toBe([{name: 'person', position:'position', employees:[]}].toString());
});
