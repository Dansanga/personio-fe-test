import React, { Component } from 'react';
import './App.css';

import { OrgListContainer, Uploader } from './components/components';
import { Util } from './shared/shared';

class App extends Component {
    constructor (props) {
        super(props);
        const  orgList = {
            'Name': {
                position: 'Position',
                employees: [
                    {
                        'Employee 1': {
                            position: 'Position 1',
                            employees: []
                        }
                    },
                    {
                        'Employee 2': {
                            position: 'Position 2',
                            employees:[]
                        }

                    }
                ]
            }
        };

        this.state = {
            orgList: Util.flattenStructure([orgList]),
            draggedItem: null,
            repaint: this.repaint.bind(this),
            canDrop: true,
            clearDraggedItem: null
        };
    }

    repaint(){
        this.forceUpdate();
    }

    render() {
        return (
          <div className="App">
            <header className="App-header">
                <Uploader parentState={this.state} />
            </header>
            <p className="App-intro">
            </p>
            <OrgListContainer parentState= {this.state} items={this.state.orgList} />
          </div>
        );
    }
}

export default App;
