import React, {Component} from 'react';
import { OrgListContainer } from '../components';
import './OrgListItem.css';

/**
 * Org List Item Component
 */
class OrgListItem extends Component {
    constructor(props) {
        super(props);
        // flags
        this.toggleVisibility = this.toggleVisibility.bind(this);

        // event handlers
        this.handleDrop = this.handleDrop.bind(this);
        this.handleDragOver = this.handleDragOver.bind(this);
        this.handleDragStart = this.handleDragStart.bind(this);

        // local state object
        this.state = {
            hideEmployees: false,
            hideSelf: false
        };
    }

    render() {
        // flag if item has employees
        let hasEmployees = !!this.props.item.employees.length;
        // build -/+ icon button
        let toggleIcon = hasEmployees ? ((this.state.hideEmployees ? <button className="toggleIcon">+</button> :
            <button className="toggleIcon">-</button>)) : " ";
        return (
            <div className="orgListItemWrapper" draggable="true" onDragOver={this.handleDragOver}
                 onDrop={this.handleDrop} onDragStart={this.handleDragStart}>
                <div className="orgListItemHeader" onClick={this.toggleVisibility}>
                    {toggleIcon}
                    {this.props.item.name}
                </div>
                <div className="orgListItemBody">
                    {this.props.item.position}
                    {
                        // Output only if the current item has employees
                        !this.state.hideEmployees &&
                        hasEmployees &&
                        // Every item has a container within it to display employees
                        <OrgListContainer parentState={this.props.parentState} items={this.props.item.employees}/>
                    }
                </div>
            </div>
        );
    }

    /**
     * Invert flag value to show employees when item is collapsed/expanded
     */
    toggleVisibility() {
        this.setState({
            hideEmployees: !this.state.hideEmployees
        });
    }


    /**
     * Handle when an item is dropped to reorganize the hierarchy
     * @param event
     */
    handleDrop(event) {
        event.stopPropagation();
        if (this.props.parentState.draggedItem !== this.props.item && this.props.parentState.canDrop) {
            this.props.item.employees.push(this.props.parentState.draggedItem);
            this.props.parentState.draggedItem = null;
            this.props.parentState.clearDraggedItem();
            this.props.parentState.clearDraggedItem = null;
            this.props.parentState.repaint();
        }
    }

    /**
     * This code is necessary to enable drag and dropping and to prevent dropping items into their children
     * @param event
     */
    handleDragOver(event) {
        // enable drag and drop
        event.preventDefault();

        // Block if the item being dragged is in the hirearchy of the target where it is being dropped
        if(this.props.parentState.draggedItem === this.props.item){
            event.stopPropagation(); // propagation is needed to traverse the hierarchy
            this.props.parentState.canDrop = false; // flag to block insertion
        } else {
            this.props.parentState.canDrop= true; // clear flag if there is no conflict
        }
    }

    /**
     * Set as the item being dragged and provide mechanism to remove from the current list position after dropped
     * @param event
     */
    handleDragStart(event) {
        event.stopPropagation();
        // Set as item being dragged
        this.props.parentState.draggedItem = this.props.item;
        // Callback to remove dragged item from the target item's drop event
        this.props.parentState.clearDraggedItem = (() => {
            this.props.list.splice(this.props.list.indexOf(this.props.item), 1);
        });
    }
}

export default OrgListItem;