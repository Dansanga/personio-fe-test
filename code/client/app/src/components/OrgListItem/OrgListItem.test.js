import React from 'react';
import ReactDOM from 'react-dom';
import OrgListItem from './OrgListItem';

it('renders without crashing', () => {
    let sampleItem = {name:'person', position:'position', employees:[]};
    const div = document.createElement('div');
    ReactDOM.render(<OrgListItem item={sampleItem} />, div);
});
