import React, { Component } from 'react';
import { OrgListItem } from '../components';
import './OrgListContainer.css';

/**
 * Org List Container Component
 */
class OrgListContainer extends Component {
    render () {
        // Iterate List to create Items using OrgListItem components
        const listItems = (!this.props.items || !this.props.items.length) ? [] : this.props.items.map((item) => {
            return (
                <li key={item.name}>
                    <OrgListItem parentState={this.props.parentState} list={this.props.items} item={item}/>
                </li>
            );
        });
        // render the wrapper and content
        return(
            <ul className="orgListContainerWrapper">
                {listItems}
            </ul>
        );
    }
}

export default OrgListContainer