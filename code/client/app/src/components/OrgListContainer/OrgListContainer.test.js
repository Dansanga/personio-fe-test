import React from 'react';
import ReactDOM from 'react-dom';
import OrgListContainer from './OrgListContainer';

it('renders without crashing', () => {
    let sampleList = [{name:'person', position:'position', employees:[]}];
    const div = document.createElement('div');
    ReactDOM.render(<OrgListContainer items={sampleList} />, div);
});
