import React, { Component } from 'react';
import './Uploader.css';

import { Util } from '../../shared/shared';

/**
 * Uploader component to allow drag and drop reading of org chart text files.
 */
class Uploader extends Component {
    constructor(props) {
        super(props);
        this.handleDragOver = this.handleDragOver.bind(this);
        this.handleDrop = this.handleDrop.bind(this);
    }

    render() {
        return(
            <div className="uploadTarget" onDragOver={this.handleDragOver} onDrop={this.handleDrop}>
                    Drag and Drop a JSON File to Load
            </div>
        )
    }

    /**
     * This code is required to enable drag and drop
     * @param event
     */
    handleDragOver(event) {
        event.preventDefault();
        event.stopPropagation();
    }

    /**
     * Read File and trigger org chart repaint with new data
     * @param event
     */
    handleDrop(event) {
        // setup code
        event.stopPropagation();
        event.preventDefault();
        // Capture dropped file
        let file = event.dataTransfer.files[0];
        let reader = new FileReader();
        reader.onload = (event) => {
            // Parse read data and trigger org chard repaint
            try { // protect against invalid json
                let orgList = JSON.parse(event.target.result);
                this.props.parentState.orgList = Util.flattenStructure([orgList]);
                this.props.parentState.repaint();
            } catch(err) {
                alert('File cannot be read. Check that it in a text format and the content is valid.');
            }

        };
        // Trigger file reading and onload callback
        reader.readAsText(file);
    }
}

export default Uploader;