'use strict';

// Imports
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

// Variables
const webappPath = path.join(__dirname, 'public');
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(webappPath)); // Include path for static resources.

app.use(require('./routes.js')); // Including routing file of all the controllers.

// Port to listen on
const port = process.env.PORT || 3300;

// Information message on node version details
console.log(`Using versions: ${JSON.stringify(process.versions)}`);

// Serve application on app root get method
app.get('/', function (req, res, next) {
  console.log(`loading from path ${webappPath}`);

  let ext = req.originalUrl.split('.').pop();
  console.log(ext);

  if (ext === 'js' || ext === 'css') {
    next();
  } else {
    res.sendFile(path.join(webappPath, 'index.html'));
  }
});

// Start server listening
const server = app.listen(port, function () {
  console.log('Server listening on port ' + port);
});

app.close = () => {
  server.close();
};

module.exports = app;
