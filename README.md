# README #
Front End Test for Personio

#Running the Pre-Built Application#

##In the provided server##
1. Install the latest version of Node.js
2. In a terminal window:
   1. Go to code/server
   2. Run "npm install"
   3. Run "node server.js"
3. Point a bowser window to "http://localhost:3300"
4. Sample JSON can be found in code/client/app/data

##On your own server##
1. Go to code/server/public
2. Copy all files to your web server of choice
3. Open in a browser window
4. Sample JSON file can be found in code/client/app/data

#Building the Application#
1. Install the latest version of Node.js
2. In a terminal window:
   1. Go to code/client/app
   2. run "npm install -g create-react-app"
   3. run "npm install"
   4. to develop run "npm start" and follow on screen instructions
   5. to test run "npm test" and follow on screen instructions
   6. to build run "npm run build" and follow on screen instructions